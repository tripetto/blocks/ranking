/** Dependencies */
import {
    Collection,
    Forms,
    Slots,
    alias,
    created,
    definition,
    deleted,
    editor,
    insertVariable,
    isString,
    name,
    pgettext,
    score,
} from "@tripetto/builder";
import { Ranking } from "./index";
import { IOption } from "../runner";

export class Option extends Collection.Item<Ranking> implements IOption {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    description?: string;

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @created
    @deleted
    defineSlot(): void {
        this.ref.defineSlot();
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:ranking", "Name"),
            form: {
                title: pgettext("block:ranking", "Option name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:ranking", "Description"),
            form: {
                title: pgettext("block:ranking", "Description"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "description", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:ranking", "Options"));

        this.editor.option({
            name: pgettext("block:ranking", "Identifier"),
            form: {
                title: pgettext("block:ranking", "Identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:ranking",
                            "If an option identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score:1",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:ranking", "Score"),
            form: {
                title: pgettext("block:ranking", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}

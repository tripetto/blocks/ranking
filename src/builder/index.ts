/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    isNumber,
    isString,
    npgettext,
    pgettext,
    slots,
    supplies,
    tripetto,
} from "@tripetto/builder";
import { Option } from "./option";
import { PositionCondition } from "./conditions/position";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SELECTED from "../../assets/selected.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:ranking", "Ranking");
    },
})
export class Ranking extends NodeBlock {
    @definition("items")
    @affects("#name")
    @supplies("#slot", "position:*")
    readonly options = Collection.of<Option, Ranking>(Option, this);

    @definition("boolean", "optional")
    @affects("#required")
    @affects("#slots")
    required?: boolean;

    @definition("number", "optional")
    @affects("#slots")
    max?: number;

    @definition("string", "optional")
    @affects("#label")
    @affects("#slots")
    alias?: string;

    @definition("boolean", "optional")
    @affects("#slots")
    exportable?: boolean;

    @definition("boolean", "optional")
    randomize?: boolean;

    @definition("string", "optional")
    @affects("#slots")
    format?: "fields" | "concatenate" | "both";

    @definition("string", "optional")
    formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";

    @definition("string", "optional")
    formatSeparatorCustom?: string;

    get label() {
        return npgettext(
            "block:ranking",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.options.count,
            this.type.label
        );
    }

    @slots
    defineSlot(): void {
        let count = this.max || this.options.count;
        let i = 1;
        const score = this.slots.select<Slots.Numeric>("score", "feature");

        while (count > 0) {
            const sequence = i - 1;

            this.slots.dynamic({
                type: Slots.String,
                reference: `position:${i}`,
                label: `#${i}`,
                sequence,
                required: this.required,
                alias: (this.alias && `${this.alias} #${i}`) || undefined,
                exportable: this.format !== "concatenate" && this.exportable,
                pipeable: {
                    label: pgettext("block:ranking", "Ranking"),
                    alias: this.alias,
                },
            });

            if (score) {
                const ref = this.slots.feature({
                    type: Slots.Numeric,
                    reference: `score:${i}`,
                    label: pgettext("block:ranking", "Score #%1", `${i}`),
                    sequence,
                    exportable: score.exportable,
                    alias: (score.alias && `${score.alias} #${i}`) || undefined,
                    protected: true,
                });

                ref.precision = score.precision;
                ref.digits = score.digits;
                ref.decimal = score.decimal;
                ref.separator = score.separator;
                ref.prefix = score.prefix;
                ref.prefixPlural = score.prefixPlural;
                ref.suffix = score.suffix;
                ref.suffixPlural = score.suffixPlural;
            } else {
                this.slots.delete(`score:${i}`, "feature");
            }

            count--;
            i++;
        }

        while (this.slots.select(`position:${i}`, "dynamic")) {
            this.slots.delete(`position:${i}`, "dynamic");
            this.slots.delete(`score:${i}`, "feature");

            i++;
        }

        if (this.format === "concatenate" || this.format === "both") {
            this.slots.feature({
                type: Slots.Text,
                reference: "concatenation",
                label: pgettext("block:ranking", "Text value"),
                exportable: this.exportable,
                alias: this.alias,
                protected: true,
            });
        } else {
            this.slots.delete("concatenation", "feature");
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        const collection = this.editor.collection({
            collection: this.options,
            title: pgettext("block:ranking", "Ranking options"),
            icon: ICON_SELECTED,
            placeholder: pgettext("block:ranking", "Unnamed option"),
            sorting: "manual",
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            markdown:
                Markdown.MarkdownFeatures.Formatting |
                Markdown.MarkdownFeatures.Hyperlinks,
            emptyMessage: pgettext(
                "block:ranking",
                "Click the + button to add an option..."
            ),
            onResize: (ref) => {
                scores.disabled(!ref.count);
                max.max(ref.count);
            },
        });

        this.editor.groups.settings();

        const max = new Forms.Numeric(
            Forms.Numeric.bind(this, "max", undefined, this.options.count)
        )
            .label(
                pgettext(
                    "block:ranking",
                    "Specify the maximum number of ranking positions."
                )
            )
            .min(1)
            .max(this.options.count)
            .width(75);

        this.editor.option({
            name: pgettext("block:ranking", "Limits"),
            form: {
                title: pgettext("block:ranking", "Limits"),
                controls: [max],
            },
            activated: isNumber(this.max),
        });

        this.editor.option({
            name: pgettext("block:ranking", "Randomization"),
            form: {
                title: pgettext("block:ranking", "Randomization"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:ranking",
                            "Randomize the options (using [Fisher-Yates shuffle](%1))",
                            "https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle"
                        ),
                        Forms.Checkbox.bind(this, "randomize", undefined, true)
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.randomize),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();

        const scores = this.editor
            .scores({
                target: this,
                collection,
                label: pgettext("block:ranking", "Score (total)"),
                description: pgettext(
                    "block:ranking",
                    "Generates scores for the ranking. Open the settings panel for each option to set the individual score for that option."
                ),
                onChange: () => this.defineSlot(),
            })
            .disabled(!this.options.count);

        this.editor.alias(this);

        const formatSeparatorCustom = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "formatSeparatorCustom", undefined)
        )
            .visible(this.formatSeparator === "custom")
            .sanitize(false)
            .width(200)
            .label(pgettext("block:ranking", "Use this separator:"));

        const formatSeparatorOptions = new Forms.Group([
            new Forms.Dropdown<
                | "comma"
                | "space"
                | "list"
                | "bullets"
                | "numbers"
                | "conjunction"
                | "disjunction"
                | "custom"
            >(
                [
                    {
                        label: pgettext("block:ranking", "Comma separated"),
                        value: "comma",
                    },
                    {
                        label: pgettext("block:ranking", "Space separated"),
                        value: "space",
                    },
                    {
                        label: pgettext(
                            "block:ranking",
                            "List on multiple lines"
                        ),
                        value: "list",
                    },
                    {
                        label: pgettext("block:ranking", "Bulleted list"),
                        value: "bullets",
                    },
                    {
                        label: pgettext("block:ranking", "Numbered list"),
                        value: "numbers",
                    },
                    {
                        label: pgettext(
                            "block:ranking",
                            "Language sensitive conjunction (_, _, and _)"
                        ),
                        value: "conjunction",
                    },
                    {
                        label: pgettext(
                            "block:ranking",
                            "Language sensitive disjunction (_, _, or _)"
                        ),
                        value: "disjunction",
                    },
                    {
                        label: pgettext("block:ranking", "Custom separator"),
                        value: "custom",
                    },
                ],
                Forms.Radiobutton.bind(
                    this,
                    "formatSeparator",
                    undefined,
                    "comma"
                )
            )
                .label(
                    pgettext(
                        "block:ranking",
                        "How to separate the selected options:"
                    )
                )
                .on((formatSeparator) => {
                    formatSeparatorCustom.visible(
                        formatSeparator.value === "custom"
                    );
                }),
            formatSeparatorCustom,
        ]).visible(this.format === "concatenate" || this.format === "both");

        this.editor.option({
            name: pgettext("block:ranking", "Data format"),
            form: {
                title: pgettext("block:ranking", "Data format"),
                controls: [
                    new Forms.Radiobutton<"fields" | "concatenate" | "both">(
                        [
                            {
                                label: pgettext(
                                    "block:ranking",
                                    "Every position as a separate field"
                                ),
                                description: pgettext(
                                    "block:ranking",
                                    "Every position is included in the dataset as a separate value."
                                ),
                                value: "fields",
                            },
                            {
                                label: pgettext(
                                    "block:ranking",
                                    "Single text field with the ranking"
                                ),
                                description: pgettext(
                                    "block:ranking",
                                    "The ranking is merged into a single string of text, separated by a configurable separator."
                                ),
                                value: "concatenate",
                            },
                            {
                                label: pgettext(
                                    "block:ranking",
                                    "Both options above"
                                ),
                                description: pgettext(
                                    "block:ranking",
                                    "Includes every position in the dataset together with the concatenated text."
                                ),
                                value: "both",
                            },
                        ],
                        Forms.Radiobutton.bind(
                            this,
                            "format",
                            undefined,
                            "fields"
                        )
                    )
                        .label(
                            pgettext(
                                "block:ranking",
                                "This setting determines how the data is stored in the dataset:"
                            )
                        )
                        .on((format) => {
                            formatSeparatorOptions.visible(
                                format.value === "concatenate" ||
                                    format.value === "both"
                            );
                        }),
                    formatSeparatorOptions,
                ],
            },
            activated: isString(this.format),
        });

        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        let i = 1;
        let position = this.slots.select(`position:${i}`, "dynamic");

        while (position) {
            if (position.label) {
                const group = this.conditions.group(
                    pgettext("block:ranking", "Position #%1", `${i}`),
                    ICON
                );

                this.options.each((option) => {
                    if (option.name) {
                        group.template({
                            condition: PositionCondition,
                            label: option.name,
                            icon: ICON_SELECTED,
                            burst: true,
                            props: {
                                slot: position,
                                option: option,
                            },
                        });
                    }
                });
            }

            i++;
            position = this.slots.select(`position:${i}`, "dynamic");
        }

        let score = this.slots.select<Slots.Numeric>(`score`, "feature");

        if (score) {
            const scoreGroup = this.conditions.group(
                pgettext("block:ranking", "Scores"),
                ICON_SCORE
            );
            const fnScore = (s: Slots.Numeric, separator: boolean) => {
                if (s.label) {
                    const group = scoreGroup.group(
                        s.label,
                        ICON_SCORE,
                        undefined,
                        separator
                    );

                    each(
                        [
                            {
                                mode: "equal",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is equal to"
                                ),
                            },
                            {
                                mode: "not-equal",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is not equal to"
                                ),
                            },
                            {
                                mode: "below",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is lower than"
                                ),
                            },
                            {
                                mode: "above",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is higher than"
                                ),
                            },
                            {
                                mode: "between",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is between"
                                ),
                            },
                            {
                                mode: "not-between",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is not between"
                                ),
                            },
                            {
                                mode: "defined",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is calculated"
                                ),
                            },
                            {
                                mode: "undefined",
                                label: pgettext(
                                    "block:ranking",
                                    "Score is not calculated"
                                ),
                            },
                        ],
                        (condition: { mode: TScoreModes; label: string }) => {
                            group.template({
                                condition: ScoreCondition,
                                label: condition.label,
                                autoOpen:
                                    condition.mode !== "defined" &&
                                    condition.mode !== "undefined",
                                props: {
                                    slot: s,
                                    mode: condition.mode,
                                    value: 0,
                                    to:
                                        condition.mode === "between" ||
                                        condition.mode === "not-between"
                                            ? 0
                                            : undefined,
                                },
                            });
                        }
                    );
                }
            };

            fnScore(score, false);

            i = 1;
            score = this.slots.select<Slots.Numeric>(`score:${i}`, "feature");

            while (score) {
                fnScore(score, i === 1);

                i++;
                score = this.slots.select<Slots.Numeric>(
                    `score:${i}`,
                    "feature"
                );
            }
        }
    }
}

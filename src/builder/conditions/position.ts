/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Markdown,
    affects,
    collection,
    editor,
    isFilledString,
    markdownifyToString,
    pgettext,
    tripetto,
} from "@tripetto/builder";
import { Ranking } from "..";
import { Option } from "../option";

/** Assets */
import ICON from "../../../assets/icon.svg";
import ICON_SELECTED from "../../../assets/selected.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:ranking", "Chosen option");
    },
})
export class PositionCondition extends ConditionBlock {
    @affects("#name")
    @affects("#condition")
    @collection("#options")
    option: Option | undefined;

    get title() {
        return this.node?.label || PositionCondition.label;
    }

    get label() {
        return (
            (this.slot?.label &&
                pgettext("block:ranking", "Position %1", this.slot.label)) ||
            PositionCondition.label
        );
    }

    get name() {
        return (
            (this.option
                ? this.option.name
                : pgettext("block:ranking", "Empty")) || this.type.label
        );
    }

    get icon() {
        return this.option ? ICON_SELECTED : ICON;
    }

    get options() {
        return (
            (this.node &&
                this.node.block instanceof Ranking &&
                this.node.block.options) ||
            undefined
        );
    }

    @editor
    defineEditor(): void {
        if (this.node && this.options) {
            const options: Forms.IOption<Option | undefined>[] = [];

            this.options.each((option: Option) => {
                if (isFilledString(option.name)) {
                    options.push({
                        label: markdownifyToString(
                            option.name,
                            Markdown.MarkdownFeatures.None
                        ),
                        value: option,
                    });
                }
            });

            this.editor.form({
                title:
                    (this.slot?.label &&
                        pgettext(
                            "block:ranking",
                            "Position %1",
                            this.slot.label
                        )) ||
                    PositionCondition.label,
                controls: [
                    new Forms.Dropdown<Option | undefined>(
                        options,
                        Forms.Dropdown.bind(this, "option", undefined)
                    ),
                ],
            });
        }
    }
}

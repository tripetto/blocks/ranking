/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class PositionCondition extends ConditionBlock<{
    readonly option: string | undefined;
}> {
    @condition
    isSelected(): boolean {
        const positionSlot = this.valueOf<string>();

        return (
            (positionSlot && positionSlot.reference === this.props.option) ||
            false
        );
    }
}

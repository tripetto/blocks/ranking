/** Dependencies */
import {
    MarkdownFeatures,
    NodeBlock,
    Slots,
    Str,
    Value,
    each,
    findFirst,
    markdownifyToString,
} from "@tripetto/runner";
import { IOption } from "./option";

export abstract class Ranking extends NodeBlock<{
    options?: IOption[];
    required?: boolean;
    randomize?: boolean;
    formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";
    formatSeparatorCustom?: string;
}> {
    /** Contains the randomized options order. */
    private randomized?: {
        readonly index: number;
        readonly id: string;
    }[];

    /** Contains the concatenation slot. */
    readonly concatenationSlot = this.valueOf<string, Slots.Text>(
        "concatenation",
        "feature"
    );

    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Retrieves if the ranking is required. */
    get required(): boolean {
        return this.props.required || false;
    }

    /** Retrieves the slots. */
    get slots(): Value<string, Slots.String>[] {
        const slots: Value<string, Slots.String>[] = [];
        const fnValue = (index: number) =>
            this.valueOf<string>(`position:${index}`, "dynamic", {
                confirm: true,
                onChange: (value) => {
                    this.transform();
                    this.score(index, value.reference);
                },
            });
        let i = 1;
        let slot = fnValue(i);

        while (slot) {
            slots.push(slot);
            i++;
            slot = fnValue(i);
        }

        return slots;
    }

    private transform(): void {
        if (this.concatenationSlot) {
            const list: string[] = [];
            let s = "";
            let n = 0;

            each(this.slots, (slot) => {
                const label = slot.value || "";

                if (label) {
                    switch (this.props.formatSeparator) {
                        case "space":
                            s += (s === "" ? "" : " ") + label;
                            break;
                        case "list":
                            s += (s === "" ? "" : "\n") + label;
                            break;
                        case "bullets":
                            s += (s === "" ? "" : "\n") + "- " + label;
                            break;
                        case "numbers":
                            s += (s === "" ? "" : "\n") + `${++n}. ${label}`;
                            break;
                        case "conjunction":
                        case "disjunction":
                            list.push(label);
                            break;
                        case "custom":
                            s +=
                                (s === ""
                                    ? ""
                                    : this.props.formatSeparatorCustom || "") +
                                label;
                            break;
                        default:
                            s += (s === "" ? "" : ", ") + label;
                            break;
                    }
                }
            });

            if (
                this.props.formatSeparator === "conjunction" ||
                this.props.formatSeparator === "disjunction"
            ) {
                try {
                    const formatter = new Intl.ListFormat(
                        this.context.l10n.current || "en",
                        { type: this.props.formatSeparator }
                    );

                    s = formatter.format(list);
                } catch {
                    s = Str.iterateToString(list, ", ");
                }
            }

            this.concatenationSlot.set(s);
        }
    }

    private score(index: number, reference: string | undefined): void {
        const score = this.valueOf<number, Slots.Numeric>(
            `score:${index}`,
            "feature",
            {
                onChange: () => {
                    let n: number | undefined = undefined;

                    if (this.scoreSlot) {
                        let i = 1;
                        let s = this.valueOf<number, Slots.Numeric>(
                            `score:${i}`,
                            "feature"
                        );

                        while (s) {
                            if (s.hasValue) {
                                n = (n || 0) + s.value;
                            }

                            i++;
                            s = this.valueOf<number, Slots.Numeric>(
                                `score:${i}`,
                                "feature"
                            );
                        }

                        this.scoreSlot.set(n);
                    }
                },
            }
        );

        if (score) {
            score.set(
                reference
                    ? findFirst(
                          this.props.options,
                          (option) => option.id === reference
                      )?.score || 0
                    : undefined
            );
        }
    }

    /** Retrieves the options. */
    options<T>(props: {
        readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => T;
    }): (Omit<IOption, "description"> & {
        readonly label: T;
        readonly description?: T;
    })[] {
        const options =
            this.props.options?.map((option) => ({
                id: option.id,
                name: markdownifyToString(
                    option.name,
                    this.context,
                    undefined,
                    false,
                    MarkdownFeatures.Formatting | MarkdownFeatures.Hyperlinks
                ),
                label: props.markdownifyToJSX(option.name, false),
                description:
                    (option.description &&
                        props.markdownifyToJSX(option.description, true)) ||
                    undefined,
                value: option.value,
                score: option.score,
            })) || [];

        if (this.props.randomize && options.length > 1) {
            if (
                !this.randomized ||
                this.randomized.length !== options.length ||
                findFirst(
                    this.randomized,
                    (option) => options[option.index]?.id !== option.id
                )
            ) {
                this.randomized = options.map((option, index) => ({
                    index,
                    id: option.id,
                }));

                let length = this.randomized.length;

                while (--length) {
                    const index = Math.floor(Math.random() * length);
                    const temp = this.randomized[length];

                    this.randomized[length] = this.randomized[index];
                    this.randomized[index] = temp;
                }
            }

            return this.randomized.map((option) => options[option.index]);
        } else if (this.randomized) {
            this.randomized = undefined;
        }

        return options;
    }
}

// As long as the typings for Intl are incomplete, we need to declare them ourselves.
// See https://github.com/microsoft/TypeScript/issues/46907
// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Intl {
    class ListFormat {
        constructor(
            locales?: string | string[],
            options?: {
                type?: "conjunction" | "disjunction";
            }
        );
        format(values: string[]): string;
    }
}
